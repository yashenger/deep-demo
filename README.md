# deep-demo

#### 1. 介绍

各位，我做了精美的demo，供大家学习，后续继续添加，请各位学习。demo短小精悍，从若依框架中提取而成，

从0基础进入企业框架开发的世界。



##### 1.1 vueDemo

vue前端：
1.纯HTML+ vue.js 
2.纯HTML+ vue.js +config.js访问controller

3.用脚手架搭建VUE
    https://blog.csdn.net/tinalucky/article/details/104239960

4.基于数据库的原生访问。

5.elementUI的引入，实现单表的增删改查

##### 1.2  分页pageDemo

##### 1.3 权限securityDemo

1.4 注解AnnotationDemo

#### 2. 基本Git命令使用

##### 2.1 git remote -v  查看当前的分支

```
origin  https://gitee.com/liuxinfeng/deep-demo.git (fetch)
origin  https://gitee.com/liuxinfeng/deep-demo.git (push)
```



##### 2.2 git branch -a  查看所有的分支

#####  2.3 git branch 查看当前分支

*表示当前正在使用的分值

 * ```
      master
   
    * securityDemo
   ```

   

##### 2.4 git checkout  切换分支



* master

  ```
  remotes/origin/HEAD -> origin/master
  remotes/origin/demo1
  remotes/origin/demo2
  remotes/origin/demo4
  remotes/origin/master
  remotes/origin/securityDemo
  ```

  





#### 3. 在线帮助文档

https://docs.qq.com/doc/p/f3c21447befa2acad08f6bfeb70ba1c46aa9937c?dver=2.1.27237808




